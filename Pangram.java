package practice;

import java.util.TreeSet;
import java.util.HashMap;
import java.util.NavigableSet;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;
import java.util.Iterator;

/**
 * Finds a short (but not necessarily shortest) pangram from dictionary.
 */
public class Pangram {
  
  private static NavigableSet<String> words;
  
  public static void loadWords() throws IOException
  {
    words = new TreeSet<String>();
    
    BufferedReader reader = new BufferedReader(
        new FileReader("/usr/share/dict/words"));
    
    String nextWord;
    
    while ((nextWord = reader.readLine()) != null)
    {
      if (nextWord.length() > 1)
      {
        words.add(nextWord.toLowerCase());
      }
    }
    
    words.add("a");
    words.add("i");
    
    reader.close();
  }
  
  public static void pangram()
  {
    Stack<String> stack = new Stack<String>();
    
    NavigableSet<String> localWords = words;
    String next;
    String shortest = null;
    HashMap<Character, Integer> counts = new HashMap<Character, Integer>();
    int count;
    
    while (true)
    {
      next = localWords.first();
      String bestNext = null;
      double bestRatio = 0;
      // Find the word with the best ratio of added unique characters to length.
      for (Iterator<String> it = localWords.iterator(); it.hasNext(); next = it.next())
      {
        count = counts.size();
        
        addWordToCount(counts, next);
        stack.push(next);
        
        int increase = counts.size() - count;
        double ratio = (double)increase/(double) next.length();
        if (bestNext == null || ratio > bestRatio)
        {
          bestNext = next;
          bestRatio = ratio;
        }
        
        removeWordFromCount(counts, next);
        stack.pop();
      }
      
      if (bestNext != null)
      {
        addWordToCount(counts, bestNext);
        stack.push(bestNext);
        
        String candidate = printStack(stack);
        
        if (counts.size() >= 26)
        {
          if ((shortest == null || candidate.length() <= shortest.length()))
          {
            // Things improved without the last removal.
            System.out.println(candidate);
            shortest = candidate;
            break;
          }
        }
      }
    }
  }
  
  public static String printStack(Stack<String> stack)
  {
    StringBuilder sb = new StringBuilder();
    for (String str : stack)
    {
      sb.append(str + " ");
    }
    
    return sb.toString();
  }
  
  public static void addWordToCount(HashMap<Character, Integer> counts, String word)
  {
    for (char c : word.toCharArray())
    {
      if (c >= 'a' && c <= 'z')
      {
        if (counts.containsKey(c))
        {
          counts.put(c, counts.get(c) + 1);
        }
        else
        {
          counts.put(c, 1);
        }
      }
    }
  }
  
  public static void removeWordFromCount(HashMap<Character, Integer> counts, String word)
  {
    for (char c : word.toCharArray())
    {
      if (c != ' ' && c != '-' && c != '\'')
      {
        if (counts.containsKey(c))
        {
          counts.put(c, counts.get(c) - 1);
          if (counts.get(c) == 0)
          {
            counts.remove(c);
          }
        }
      }
    }
  }
  
  public static void main(String[] unused_args) throws IOException
  {
    loadWords();
    pangram();
  }
}

